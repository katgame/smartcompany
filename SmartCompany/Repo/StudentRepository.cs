﻿using SmartCompany.Entities;
using SmartCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartCompany.Repo
{
    public class StudentRepository : PersonRepository
    {
        private MyCompanyDBContext _context;

        public StudentRepository()
        {

        }

        public StudentRepository(MyCompanyDBContext context)
        {
            _context = context;
        }

        public override void UpdatePerson(PersonModel person)
        {
            base.UpdatePerson(person);
        }

        public void AddStudent(StudentModel student)
        {

        }

        public virtual void UpdateStudent(StudentModel student)
        {

        }

        public void UpdateStudentCourse(StudentModel student)
        {

        }
    }
}
