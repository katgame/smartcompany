import { Component, OnInit } from '@angular/core';
import Chart from 'chart.js';
import { UserService } from '../../services/user.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { PersonDetailsComponent } from '../person-details/person-details.component';
import { Router } from '@angular/router';

@Component({
  selector: 'dashboard-cmp',
  moduleId: module.id,
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {
  public dataRows: [] = [];
  public headerRow: string[] = [];

  preventSingleClick = false;
  timer: any;
  delay: Number;

  constructor(private router: Router, private userService: UserService, public dialog: MatDialog) { }

  ngOnInit() {
    this.GetPersonList();
    this.headerRow = ['Name', 'Surname', 'DateOfBirth', 'Role', 'SystemGeneratedEmpNR'];
  }

  GetPersonList() {
    this.userService.GetPersonList().subscribe(
      (result: any) => {
        this.dataRows = result;
      }
    );
  }

  doubleClick(data: PersonModel) {
    this.preventSingleClick = true;
    clearTimeout(this.timer);
    this.router.navigate(['persondetails', { id: data.id }]);
  }
}

declare interface TableData {
  headerRow?: string[];
  dataRows?: [];
}

export class PersonModel {
  id: number;
  Name: string;
  Surname: string;
  DateOfBirth: string;
  Role: string;
  SystemGeneratedEmpNR: string;
}
