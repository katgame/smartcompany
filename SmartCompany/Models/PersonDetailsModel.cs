﻿using SmartCompany.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartCompany.Models
{
    public class PersonDetailsModel : PersonModel
    {
        public List<PersonAudit> PersonAudits { get; set; }
    }
}
