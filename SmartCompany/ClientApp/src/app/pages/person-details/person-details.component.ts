import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Pipe, PipeTransform } from '@angular/core'
import * as moment from 'moment'

@Component({
  selector: 'person-details-cmp',
  moduleId: module.id,
  templateUrl: 'person-details.component.html'
})

export class PersonDetailsComponent implements OnInit {
  public personProfile: PersonProfile = new PersonProfile();
  public personProfileForm: FormGroup;
  public submitted: boolean = false;

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router, private formBuilder: FormBuilder) {
    this.route.params.subscribe(params => {
      this.GetPersonDetails(params.id as number)
    });
  }

  ngOnInit() {
    this.personProfileForm = this.formBuilder.group({
      Name: [''],
      Surname: [''],
      DateOfBirth: [''],
      Role: [''],
      SystemGeneratedEmpNR: ['']
    });
  }

  get f() { return this.personProfileForm.controls; }

  GetPersonDetails(id: number) {
    this.userService.GetPersonDetails(id).subscribe(
      (result: any) => {
        this.personProfile = result;
        this.personProfileForm.controls['Name'].setValue(this.personProfile.name);
        this.personProfileForm.controls['Surname'].setValue(this.personProfile.surname);
        this.personProfileForm.controls['DateOfBirth'].setValue(moment(new Date(this.personProfile.dateOfBirth)).format('DD/MM/YYYY'));
        this.personProfileForm.controls['Role'].setValue(this.personProfile.role);
        this.personProfileForm.controls['SystemGeneratedEmpNR'].setValue(this.personProfile.systemGeneratedEmpNR);
      }
    );
  }

  BackClick() {
    this.router.navigate(['dashboard']);
  }

  onSubmit() {
    this.submitted = true;

    if (this.personProfileForm.invalid) {
      return;
    }

    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.personProfileForm.value, null, 4));
  }

  onReset() {
    this.submitted = false;
    this.personProfileForm.reset();
  }
}

export class PersonProfile {
  id: number;
  name: string;
  surname: string;
  dateOfBirth: Date;
  role: string;
  systemGeneratedEmpNR: string;
  personAudits: PersonAudit[]
}

export class PersonAudit {
  id: number;
  surnameOld: string;
  surnameNew: string;
  modifiedOn: Date;
  modifiedByName: string;
}

