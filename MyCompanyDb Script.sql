USE [MyCompanyDB]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ReferredBy] [int] NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SystemGeneratedEmpNr] [varchar](100) NOT NULL,
	[StartDate] [date] NOT NULL,
	[Role] [varchar](250) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[PersonId] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoices]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoices](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BillingDate] [date] NOT NULL,
	[CustomerId] [int] NOT NULL,
 CONSTRAINT [PK_Invoices] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Person]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Person](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](250) NOT NULL,
	[Surname] [varchar](250) NOT NULL,
	[DateOfBirth] [date] NOT NULL,
	[Modified_By] [int] NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonAudit]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonAudit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Surname_Old] [varchar](250) NOT NULL,
	[Surname_New] [varchar](250) NOT NULL,
	[Modified_On] [datetime] NOT NULL,
	[Modified_By] [int] NOT NULL,
 CONSTRAINT [PK_PersonAudit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[Customer_Invoice]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Customer_Invoice]
AS
SELECT        TOP (100) PERCENT dbo.Invoices.Id, dbo.Invoices.BillingDate, custa.Name, custb.Name AS ReferredByName
FROM            dbo.Customers AS custa INNER JOIN
                         dbo.Invoices ON custa.Id = dbo.Invoices.CustomerId INNER JOIN
                         dbo.Customers AS custb ON custa.Id = custb.ReferredBy
ORDER BY dbo.Invoices.BillingDate
GO
/****** Object:  View [dbo].[Person_Employee_V]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Person_Employee_V]
AS
SELECT        dbo.Person.Name, dbo.Person.Surname, dbo.Person.DateOfBirth, dbo.Employee.Role, dbo.Employee.SystemGeneratedEmpNr
FROM            dbo.Person WITH (NOLOCK) INNER JOIN
                         dbo.Employee WITH (NOLOCK) ON dbo.Person.Id = dbo.Employee.PersonId
WHERE        (dbo.Person.Name LIKE 'John') AND (dbo.Employee.IsActive = 1) AND (dbo.Employee.StartDate >= '01-08-2017') AND (dbo.Employee.StartDate <= '01-08-2017')
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (1, N'ABC111', CAST(N'2020-01-01' AS Date), N'Sales Administrator', 1, 1)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (2, N'ABC112', CAST(N'2020-01-02' AS Date), N'Support Technician', 1, 2)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (3, N'ABC113', CAST(N'2020-03-08' AS Date), N'Software Developer', 1, 3)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (4, N'ABC114', CAST(N'2020-08-02' AS Date), N'GP Support Specialist', 1, 4)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (5, N'ABC115', CAST(N'2017-01-08' AS Date), N'System Analyst', 1, 5)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (6, N'ABC116', CAST(N'2020-05-06' AS Date), N'Human Resource Manager', 1, 6)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (7, N'ABC117', CAST(N'2020-04-03' AS Date), N'Procurement', 1, 7)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (8, N'ABC118', CAST(N'2020-08-03' AS Date), N'Billing Clerk', 1, 8)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (9, N'ABC119', CAST(N'2020-08-04' AS Date), N'Applicaiton Tester', 1, 9)
GO
INSERT [dbo].[Employee] ([Id], [SystemGeneratedEmpNr], [StartDate], [Role], [IsActive], [PersonId]) VALUES (10, N'ABC110', CAST(N'2020-08-04' AS Date), N'Receptionist', 1, 10)
GO
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO
SET IDENTITY_INSERT [dbo].[Person] ON 
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (1, N'Danielle ', N'Barnard', CAST(N'1980-01-01' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (2, N'Helen', N'Van Rooyen', CAST(N'1981-02-02' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (3, N'Heidi', N'Wilson', CAST(N'1982-03-03' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (4, N'Bryan', N'Wood', CAST(N'1983-04-04' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (5, N'John', N'Skhosana', CAST(N'1984-05-05' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (6, N'Bongiwe', N'Van Der Berg', CAST(N'1985-06-06' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (7, N'Nicolaas', N'Modise', CAST(N'1986-07-07' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (8, N'Wesley', N'Lottering', CAST(N'1987-08-08' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (9, N'Sydney', N'Mohamed', CAST(N'1988-09-09' AS Date), NULL)
GO
INSERT [dbo].[Person] ([Id], [Name], [Surname], [DateOfBirth], [Modified_By]) VALUES (10, N'Bonginkosi', N'Dlamini', CAST(N'1869-08-06' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Person] OFF
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [FK_ParentId] FOREIGN KEY([ReferredBy])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [FK_ParentId]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Person] FOREIGN KEY([PersonId])
REFERENCES [dbo].[Person] ([Id])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Person]
GO
ALTER TABLE [dbo].[Invoices]  WITH CHECK ADD  CONSTRAINT [FK_Invoices_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Invoices] CHECK CONSTRAINT [FK_Invoices_Customers]
GO
/****** Object:  StoredProcedure [dbo].[Employee_INS_SP]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Employee_INS_SP] (
	@SystemGeneratedEmpNr VARCHAR(100),
	@StartDate DATE,
	@ROLE VARCHAR(250),
	@IsActive BIT,
	@PersonId INT
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		INSERT INTO Employee (
			SystemGeneratedEmpNr,
			StartDate,
			ROLE,
			IsActive,
			PersonId
			)
		VALUES (
			@SystemGeneratedEmpNr,
			@StartDate,
			@ROLE,
			@IsActive,
			@PersonId
			)

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		DECLARE @ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		RAISERROR (
				@ErrorMessage,
				@ErrorSeverity,
				@ErrorState
				);
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Employee_UP_SP]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Employee_UP_SP] (
	@Id INT,
	@SystemGeneratedEmpNr VARCHAR(100),
	@StartDate DATE,
	@ROLE VARCHAR(250),
	@IsActive BIT
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		UPDATE Employee 
		SET
			SystemGeneratedEmpNr = @SystemGeneratedEmpNr,
			StartDate = @StartDate,
			ROLE = @ROLE,
			IsActive = @IsActive
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		DECLARE @ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		RAISERROR (
				@ErrorMessage,
				@ErrorSeverity,
				@ErrorState
				);
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Person_INS_SP]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Person_INS_SP] (
	@Name VARCHAR(250),
	@Surname VARCHAR(250),
	@DateOfBirth DATE
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		INSERT INTO Person (
			Name,
			Surname,
			DateOfBirth
			)
		VALUES (
			@Name,
			@Surname,
			@DateOfBirth
			)

		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		DECLARE @ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		RAISERROR (
				@ErrorMessage,
				@ErrorSeverity,
				@ErrorState
				);
	END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Person_UP_SP]    Script Date: 2020/09/16 17:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Person_UP_SP] (
	@Id INT,
	@Modified_by INT,
	@Name VARCHAR(250),
	@Surname VARCHAR(250),
	@DateOfBirth DATE
	)
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION

		UPDATE Person 
		SET
			Name = @Name,
			Surname = @Surname,
			DateOfBirth = @DateOfBirth,
			Modified_by = @Modified_by
		WHERE Id = @Id
		
		COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		DECLARE @ErrorMessage NVARCHAR(4000),
			@ErrorSeverity INT,
			@ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();

		RAISERROR (
				@ErrorMessage,
				@ErrorSeverity,
				@ErrorState
				);
	END CATCH
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "custa"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 119
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Invoices"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 119
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "custb"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 119
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Customer_Invoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Customer_Invoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Person"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Employee"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 464
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1740
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Person_Employee_V'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Person_Employee_V'
GO

CREATE NONCLUSTERED INDEX Person_Name
ON Person(Name);

CREATE NONCLUSTERED INDEX Employee_IsActive
ON Employee(isActive);

CREATE NONCLUSTERED INDEX Employee_StartDate
ON Employee(StartDate);

--Execute after creating database.

--CREATE TRIGGER Person_Surname_UP_TRG ON Person
--AFTER UPDATE
--AS
--INSERT INTO PersonAudit (Surname_Old, Surname_New, Modified_By, Modified_On)
--SELECT i.Surname, d.Surname, i.Modified_By, GETDATE()
--FROM Inserted i
--      INNER JOIN Deleted d ON i.ID = d.ID

--CREATE TRIGGER Person_delete_TRG ON Person
--INSTEAD OF DELETE
--AS
--BEGIN
--    IF @@rowcount > 0
--    BEGIN
--        RAISERROR( 'Record cannot be deleted!', 16, 2 )
--        ROLLBACK
--    END
--END
--GO

