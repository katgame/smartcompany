﻿using SmartCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartCompany.interfaces
{
    public interface IPersonRepository
    {
        public void AddPerson(PersonModel person);

        public List<PersonModel> GetPersonList();
    }
}
