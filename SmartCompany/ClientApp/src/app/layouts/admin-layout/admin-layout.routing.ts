import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { TableComponent } from '../../pages/table/table.component';
import { PersonDetailsComponent } from '../../pages/person-details/person-details.component';

export const AdminLayoutRoutes: Routes = [
  { path: 'table', component: TableComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'persondetails', component: PersonDetailsComponent }
];
