import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  myAppUrl = '';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  constructor(private http: HttpClient) {
  }

  GetPersonList() {
    return this.http.get('/home/GetPersonList').pipe(map(result => result));
  }

  GetPersonDetails(id) {
    return this.http.post('/home/GetPersonDetails', { Id: id }, this.httpOptions).pipe(map(result => result));
  }
}   
