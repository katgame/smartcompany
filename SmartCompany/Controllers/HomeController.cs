﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SmartCompany.Models;
using SmartCompany.Repo;

namespace SmartCompany.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private PersonRepository _personRepo;
        private StudentRepository _studentRepo;

        public HomeController(ILogger<HomeController> logger, PersonRepository personRepo, StudentRepository studentRepo)
        {
            _logger = logger;
            _personRepo = personRepo;
            _studentRepo = studentRepo;
        }

        [HttpPost]
        public IActionResult AddPerson(PersonModel person)
        {
            try
            {
                _personRepo.AddPerson(person);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok();
        }

        [HttpGet]
        [Route("GetPersonList")]
        [AllowAnonymous]
        public IActionResult GetPersonList()
        {
            try
            {
                var list = _personRepo.GetPersonList();
                return Ok(list);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        [HttpPost]
        [Route("GetPersonDetails")]
        [AllowAnonymous]
        public IActionResult GetPersonDetails([FromBody] PersonModel person)
        {
            try
            {
                var list = _personRepo.GetPersonDetails(person.Id);
                return Ok(list);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
        }

        [HttpPost]
        public IActionResult AddStudent(StudentModel student)
        {
            try
            {
                _studentRepo.AddStudent(student);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
            }

            return Ok();
        }
    }
}
