﻿using SmartCompany.Entities;
using SmartCompany.interfaces;
using SmartCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartCompany.Repo
{
    public class PersonRepository : IPersonRepository
    {
        private MyCompanyDBContext _context;

        public PersonRepository()
        {

        }

        public PersonRepository(MyCompanyDBContext context)
        {
            _context = context;
        }

        public void AddPerson(PersonModel person)
        {

        }

        public List<PersonModel> GetPerson()
        {
            return _context.Person.Where(p => p.Name == "John")
                .Select(p => new PersonModel(p))
                .ToList();
        }

        public List<PersonModel> GetPersonList()
        {
            var result = _context.Person.Join(_context.Employee, post => post.Id, meta => meta.PersonId,
                            (post, meta) => new PersonModel
                            { 
                                Id = post.Id,
                                Name = post.Name,
                                Surname = post.Surname, 
                                DateOfBirth = post.DateOfBirth, 
                                Role = meta.Role,
                                SystemGeneratedEmpNR = meta.SystemGeneratedEmpNr 
                            }).ToList();

            return result;
        }

        public PersonDetailsModel GetPersonDetails(int id)
        {
            var result = _context.Person.Join(_context.Employee, post => post.Id, meta => meta.PersonId,
                           (post, meta) => new PersonDetailsModel
                           {
                               Id = post.Id,
                               Name = post.Name,
                               Surname = post.Surname,
                               DateOfBirth = post.DateOfBirth,
                               Role = meta.Role,
                               SystemGeneratedEmpNR = meta.SystemGeneratedEmpNr
                           }).Where(p => p.Id == id).FirstOrDefault();

            var list = _context.PersonAudit.Join(_context.Person, post => post.PersonId, meta => meta.Id,
                           (post, meta) => new PersonAudit
                           {
                               Id = post.Id,
                               ModifiedName = meta.Name + " " + meta.Surname,
                               SurnameOld = post.SurnameOld,
                               SurnameNew = post.SurnameNew,
                               ModifiedOn = post.ModifiedOn
                           }).Where(p => p.Id == id).ToList();

            if (list.Count > 0)
                result.PersonAudits.AddRange(list);

            return result;
        }

        public virtual void UpdatePerson(PersonModel person)
        {

        }
    }
}
